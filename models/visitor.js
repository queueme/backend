'use strict';
module.exports = (sequelize, DataTypes) => {
  const Visitor = sequelize.define('Visitor', {
    name: DataTypes.STRING,
    position: DataTypes.INTEGER,
    exited: DataTypes.BOOLEAN
  }, {});
  Visitor.associate = function(models) {
    Visitor.belongsTo(models.Queue, {
      foreignKey: 'queueId'
    })
  };
  return Visitor;
};