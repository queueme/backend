'use strict';
module.exports = (sequelize, DataTypes) => {
  const Venue = sequelize.define('Venue', {
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {});
  Venue.associate = function(models) {
    Venue.hasMany(models.Queue, {
      foreignKey: 'venueId'
    })
  };

  Venue.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
};
  return Venue;
};