'use strict';
module.exports = (sequelize, DataTypes) => {
  const Queue = sequelize.define('Queue', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    capacity: DataTypes.INTEGER,
    startHour: DataTypes.INTEGER,
    endHour: DataTypes.INTEGER
  }, {});
  Queue.associate = function(models) {
    Queue.belongsTo(models.Venue, {
      foreignKey: 'venueId'
    })
    Queue.hasMany(models.Visitor, {
        foreignKey: 'queueId'
      })
  };

  Queue.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
};

  return Queue;
};