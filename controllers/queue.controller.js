const { Venue, Queue } = require('../models');
const Sequelize = require('sequelize');
const { to, ReE, ReS } = require('../services/util.service');

const create = async function(req, res){
    let err, queue;

    let queue_info = req.body;

    [err, queue] = await to(Queue.create(queue_info));
    if(err) return ReE(res, err, 422);

    [err, queue] = await to(queue.save());
    if(err) return ReE(res, err, 422);

    let queue_json = queue.toWeb();

    return ReS(res, {queue:queue_json}, 201);
}

module.exports.create = create;

const remove = async function(req, res){
  let queue, err;
  queue = req.body.id;

  [err, queue] = await to(Queue.find({
    where: { id: queue }
  }).then((found) => {
    found.destroy()
  }).then(() => {
    return ReS(res, {message:'Deleted Queue'}, 204);
  }).catch((err) => {
    return ReE(res, 'error occured trying to delete the queue');
  }));
}
module.exports.remove = remove;