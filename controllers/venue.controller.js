const { Venue, Queue, Visitor } = require('../models');
const Sequelize = require('sequelize');
const { to, ReE, ReS } = require('../services/util.service');

const create = async function(req, res){
    let err, venue;

    let venue_info = req.body;

    [err, venue] = await to(Venue.create(venue_info));
    if(err) return ReE(res, err, 422);

    [err, venue] = await to(venue.save());
    if(err) return ReE(res, err, 422);

    let venue_json = venue.toWeb();

    return ReS(res, {venue:venue_json}, 201);
}
module.exports.create = create;

const getAll = async function(req, res){
    Venue.findAll({
      include: [{
        model: Queue,
        include: [Visitor]
       }
      ]
    }).then(found => {
      return ReS(res, {venues:found});
    });
}

module.exports.getAll = getAll;

const remove = async function(req, res){
  let venue, err;
  venue = req.body.id;

  [err, venue] = await to(Venue.find({
    where: { id: venue }
  }).then((found) => {
    found.destroy()
  }).then(() => {
    return ReS(res, {message:'Deleted Venue'}, 204);
  }).catch((err) => {
    return ReE(res, 'error occured trying to delete the venue');
  }));
}
module.exports.remove = remove;