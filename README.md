# QueueMe Backend

## Description
Backend for QueueMe App made during NW Hacks 2019

##### Routing         : Express
##### ORM Database    : Sequelize
##### Authentication  : Passport, JWT

## Installation

#### Download Code | Clone the Repo

```
git clone {repo_name}
```

#### Install Node Modules
```
npm install
```

## Authors
Aman Bhayani
Armaan Dhanji
Chris Hampu