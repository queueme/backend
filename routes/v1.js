const express 			= require('express');
const router 			= express.Router();

const UserController 	= require('../controllers/user.controller');
const VenueController = require('../controllers/venue.controller');
const HomeController 	= require('../controllers/home.controller');
const QueueController 	= require('../controllers/queue.controller');

const custom 	        = require('./../middleware/custom');

const passport      	= require('passport');
const path              = require('path');


require('./../middleware/passport')(passport)
/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({status:"success", message:"You hit the index route for our application", data:{"version_number":"v1.0.0"}})
});

router.post(    '/users',           UserController.create);                                                    // C
router.get(     '/users',           passport.authenticate('jwt', {session:false}), UserController.get);        // R
router.put(     '/users',           passport.authenticate('jwt', {session:false}), UserController.update);     // U
router.delete(  '/users',           passport.authenticate('jwt', {session:false}), UserController.remove);     // D
router.post(    '/users/login',     UserController.login);

//DUMMY ROUTES CREATED FOR FICTICIOUS COMPANY/VENUE UNTIL WE FINALIZE SCHEMA

// router.post(    '/companies',             passport.authenticate('jwt', {session:false}), CompanyController.create);                  // C
//router.get(     '/companies',          passport.authenticate('jwt', {session:false}), CompanyController.getAll);                  // R

 router.get(     '/venues',             passport.authenticate('jwt', {session:false}), VenueController.getAll);
 router.post(    '/venues',             passport.authenticate('jwt', {session:false}), VenueController.create);                  // C                  // R

 router.delete(  '/venues',           passport.authenticate('jwt', {session:false}), VenueController.remove);
 router.delete(  '/queues',           passport.authenticate('jwt', {session:false}), QueueController.remove);

 router.post(    '/queues',             passport.authenticate('jwt', {session:false}), QueueController.create);
// router.get(     '/companies/:company_id', passport.authenticate('jwt', {session:false}), custom.company, CompanyController.get);     // R
// router.put(     '/companies/:company_id', passport.authenticate('jwt', {session:false}), custom.company, CompanyController.update);  // U
// router.delete(  '/companies/:company_id', passport.authenticate('jwt', {session:false}), custom.company, CompanyController.remove);  // D

// Example Protected Route: Must pass Bearer token to get to dashboard
router.get('/dash', passport.authenticate('jwt', {session:false}),HomeController.Dashboard)

module.exports = router;
