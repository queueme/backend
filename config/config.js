module.exports = {
  "app": "development",
  "db_dialect": "postgres",
  "db_host": "localhost",
  "db_port": "5432",
  "db_name": process.env.POSTGRES_DB || "testdb2",
  "db_user": process.env.POSTGRES_USER|| "postgres",
  "db_password": process.env.POSTGRES_PASSWORD|| "root",
  "jwt_encryption": "secret",
  "jwt_expiration": "10000",
  "development": {
    "username": "postgres",
    "password": "root",
    "database": "testdb2",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "db_name": 'public'
  }
}
