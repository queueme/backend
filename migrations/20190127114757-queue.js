'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Queues',
      'venueId',
      Sequelize.INTEGER
    ).then(() => {
      return queryInterface.addConstraint('Queues', ['venueId'], {
        type: 'foreign key',
        name: 'queueVenueIdFkey',
        references: { //Required field
          table: 'Venues',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      })
    });
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeConstraint(
      'Queues',
      'queueVenueIdFkey'
    ).then(() => {
      return queryInterface.removeColumn(
        'Queues',
        'venueId'
      );
    })
  }
};
