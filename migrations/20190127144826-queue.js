'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // logic for transforming into the new state
    return queryInterface.addColumn(
      'Queues',
      'capacity',
      Sequelize.INTEGER
    ).then(() => {
      return queryInterface.addColumn(
        'Queues',
        'startHour',
        Sequelize.INTEGER
      ).then(()=>{
        return queryInterface.addColumn(
          'Queues',
          'endHour',
          Sequelize.INTEGER
        )
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    // logic for reverting the changes
    return queryInterface.removeColumn(
      'Queues',
      'capacity'
    ).then(() => {
      return queryInterface.removeColumn(
        'Queues',
        'startHour'
      ).then(() => {
        return queryInterface.removeColumn(
          'Queues',
          'endHour'
        )
      })
    })    
  }
}