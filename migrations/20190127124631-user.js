'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // logic for transforming into the new state
    return queryInterface.removeColumn(
      'Users',
      'first'
    ).then(() => {
      return queryInterface.removeColumn(
        'Users',
        'last'
      ).then(()=>{
        return queryInterface.addColumn(
          'Users',
          'fullName',
          Sequelize.STRING
        )
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    // logic for reverting the changes
    return queryInterface.addColumn(
      'Users',
      'first'
    ).then(() => {
      return queryInterface.addColumn(
        'Users',
        'last'
      ).then(() => {
        return queryInterface.removeColumn(
          'Users',
          'fullName'
        )
      })
    })    
  }
}
