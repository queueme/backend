'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Visitors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      position: {
        type: Sequelize.INTEGER
      },
      exited: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => {
      return queryInterface.addColumn(
        'Visitors',
        'queueId',
        Sequelize.INTEGER
      )}).then(() => {
      return queryInterface.addConstraint('Visitors', ['queueId'], {
        type: 'foreign key',
        name: 'visitorQueueIdFkey',
        references: { //Required field
          table: 'Queues',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      })
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint(
      'Visitors',
      'visitorQueueIdFkey'
    ).then(() => {
      return queryInterface.removeColumn(
        'Visitors',
        'queueId'
      );
    })
  }
};